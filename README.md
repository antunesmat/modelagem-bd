Neste modelo de banco de dados relacional, encontramos o tipo de relacionamento N para N 
onde o professor se relaciona com o aluno e com a sala atraves do ato de lecionar, isto é,
"o professor leciona para *aluno na sala" ou "o professor leciona na sala para aluno"; temos
aluno se relacionando com professor e com sala através do ato de lecionar, isto é, "aluno 
é lecionado pelo professor na sala" ou "aluno é lecionado na sala pelo professor"; por 
fim nesta relação temos a sala se relacionando com o professor e com aluno através do ato
de lecionar, isto é, "na sala, professor leciona para aluno" ou "na sala, aluno é lecionado
pelo professor". O professor e o aluno frequentam a sala. A sala possui o atributo composto
lugar, tendo como atributos prédio e número da sala. 





*Na imagem enviada a entidade aluno está descrita de forma incorreta. Correção: aluno.